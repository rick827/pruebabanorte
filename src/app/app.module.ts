import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HtmlCodePipe } from './htmlCode.pipe';
import { CustomSelectModule } from './commons/modules/custom-select/custom-select.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    BrowserModule, 
    CustomSelectModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [AppComponent, HtmlCodePipe],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
