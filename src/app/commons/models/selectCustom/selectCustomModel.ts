export class SelectCustomModel {
    id: number ;
    height:number;
    width:number;
    name:string;
    controls:[{
    id:number;
    name:string;
    type:string;
    attributes:{
    id:number;
    cols:string;
    style:string
    },
    options:[],
    tag:string
    }];
    designId:number;
    type:string
    }