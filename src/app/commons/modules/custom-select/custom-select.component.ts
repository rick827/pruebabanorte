import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { SelectCustomModel } from '../../models/selectCustom/selectCustomModel';
import { FormatModel } from '../../models/selectCustom/formatModel';


@Component({
  selector: 'customSelect',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.css']
})
export class CustomSelectComponent implements OnInit {

  constructor(private api : ApiService) { }

  @Input() dataSelect: any;
  dataGetWindow: SelectCustomModel[] = [];
  dataGetFormats: FormatModel[] = [];

  ngOnInit() {
    this.api.getWindow()
    .subscribe(res => {
      this.dataGetWindow = res;
      //console.log(11, this.dataGetWindow);
    }, err => {
      console.log(err);
    });

    this.api.getFormats()
    .subscribe(res => {
      this.dataGetFormats = res;
      console.log(22, this.dataGetFormats);
    }, err => {
      console.log(err);
    });


  }

}
