import { CustomSelectComponent } from './custom-select.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        CustomSelectComponent
    ],
    exports: [
        CustomSelectComponent
    ]

})
export class CustomSelectModule{} 