import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Http, HttpModule, Response } from '@angular/http';
import { SelectCustomModel } from '../models/selectCustom/selectCustomModel';
import { FormatModel } from '../models/selectCustom/formatModel';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})


export class ApiService {
  
  constructor(private http : HttpClient) { }
   httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  apiUrlWindow = "../../../assets/window.json";
  apiUrlFormats = "../../../assets/formats.json";

  // getdata(){
  //   return this.http.get('../../../assets/window.json').map(
  //     (response : Response) => 
  //         response.json()
  //     );
  // }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      return of(result as T);
    };
  }


  getWindow (): Observable<SelectCustomModel[]> {
    return this.http.get<SelectCustomModel[]>(this.apiUrlWindow)
      .pipe(
        catchError(this.handleError('getWindow', []))
      );
  }

  getFormats (): Observable<FormatModel[]> {
    return this.http.get<FormatModel[]>(this.apiUrlFormats)
      .pipe(
        catchError(this.handleError('getFormats', []))
   
              
      );
  }


}
